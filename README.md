     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Hi there! Welcome to Cloud9 IDE!

To get you started, we included a small hello world application.

1) Open the hello-world.html file

2) Click on the Preview button to open a live preview pane

3) Make some changes to the file, save, watch the preview, and have fun!

Happy coding!
The Cloud9 IDE team


## Support & Documentation

Visit http://docs.c9.io for documentation, or http://support.c9.io for support.
To watch some training videos, visit http://www.youtube.com/user/c9ide
Skill Levels - D3

 <script type="text/javascript">
  
d3.json("skills.json", function(data) {
                    var color = d3.scale.linear()
                                        .domain([0, 10])
                                        .range(["#fff","#1EAEDB"]);
                                        
                    var buttons = d3.selectAll("button")
                            .data(data)
                              .enter()
                                .append("button")
                                  .attr("class", "button")
                                  .style("color", "white")
                                  .style("background", function (d, i) { return color(d.Level) })
                                  .text(function (d){ return d.Skill })
                                  .attr("href", function (d) { return "#" +  d.Skill });
                            });
                            
  </script>