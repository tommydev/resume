jQuery(document).ready(function() {
    var offset = 220;
    var duration = 500;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
        } else {
            jQuery('.back-to-top').fadeOut(duration);
        }
    });
    
    jQuery('.back-to-top').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
});

  $(document).ready(function() {
      $(".angular").click(function() {
        $(".angular").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });

      $(".asp").click(function() {
        $(".asp").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".aws").click(function() {
        $(".aws").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".bitbucket").click(function() {
        $(".bitbucket").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".bootstrap").click(function() {
        $(".bootstrap").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".c").click(function() {
        $(".c").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".c9").click(function() {
        $(".c9").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".d3").click(function() {
        $(".d3").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".drupal").click(function() {
        $(".drupal").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".express").click(function() {
        $(".express").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".git").click(function() {
        $(".git").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".heroku").click(function() {
        $(".heroku").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".htmlcss").click(function() {
        $(".htmlcss").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".jekyll").click(function() {
        $(".jekyll").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".jquery").click(function() {
        $(".jquery").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".js").click(function() {
        $(".js").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".mongodb").click(function() {
        $(".mongodb").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".mssql").click(function() {
        $(".mssql").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".mysql").click(function() {
        $(".mysql").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".node").click(function() {
        $(".node").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".objective").click(function() {
        $(".objective").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".passbook").click(function() {
        $(".passbook").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".pgsql").click(function() {
        $(".pgsql").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".qa").click(function() {
        $(".qa").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".ruby").click(function() {
        $(".ruby").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".svn").click(function() {
        $(".svn").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".swift").click(function() {
        $(".swift").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".tfs").click(function() {
        $(".tfs").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".linux").click(function() {
        $(".linux").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".vb").click(function() {
        $(".vb").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".vs").click(function() {
        $(".vs").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".wet").click(function() {
        $(".wet").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".wp").click(function() {
        $(".wp").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
      $(".windows").click(function() {
        $(".windows").addClass("intro")
          .delay(2000)
          .queue(function() {
            $(this).removeClass("intro");
            $(this).dequeue();
          });
      });
    });
    
